﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Reflection;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using EvolutionAlgorithm.Phenotype;

namespace EvolutionAlgorithm
{
    public class Agent
    {
        public int locationX;
        public int locationY;
        public Enums.Direction direction;
        public Ph_ANN weights;
        public int gridSize = 10;
        public Flatland flatland;
        
        public Agent()
        {

        }

        public void restartPosition()
        {
            locationY = flatland.n / 2;
            locationX = flatland.n / 2;
            flatland.grid[locationX][locationY] = Enums.TileContent.Nothing;
        }
        public void turnLeft()
        {
            direction = (Enums.Direction)(((int)direction - 1 + 4) % 4);
        }
        public void turnRight()
        {
            direction = (Enums.Direction)(((int)direction + 1) % 4);
        }

        public Enums.TileContent getLeftTile()
        {
            Enums.TileContent temp = Enums.TileContent.Nothing;
            switch (direction)
            {
                case Enums.Direction.Up:
                    {
                        temp = flatland.grid[(locationX - 1 + flatland.n) % flatland.n][locationY];
                        break;
                    }
                case Enums.Direction.Right:
                    {
                        temp = flatland.grid[locationX][(locationY - 1 + flatland.n) % flatland.n];
                        break;
                    }
                case Enums.Direction.Down:
                    {
                        temp = flatland.grid[(locationX + 1) % flatland.n][locationY];
                        break;
                    }
                case Enums.Direction.Left:
                    {
                        temp = flatland.grid[locationX][(locationY + 1) % flatland.n];
                        break;
                    }
            }
            return temp;
        }
        public Enums.TileContent getRightTile()
        {
            Enums.TileContent temp = Enums.TileContent.Nothing;
            switch (direction)
            {
                case Enums.Direction.Up:
                    {
                        temp = flatland.grid[(locationX + 1) % flatland.n][locationY];
                        break;
                    }
                case Enums.Direction.Right:
                    {
                        temp = flatland.grid[locationX][(locationY + 1) % flatland.n];
                        break;
                    }
                case Enums.Direction.Down:
                    {
                        temp = flatland.grid[(locationX - 1 + flatland.n) % flatland.n][locationY];
                        break;
                    }
                case Enums.Direction.Left:
                    {
                        temp = flatland.grid[locationX][(locationY - 1 + flatland.n) % flatland.n];
                        break;
                    }
            }
            return temp;
        }
        public Enums.TileContent getForwardTile()
        {
            Enums.TileContent temp = Enums.TileContent.Nothing;
            switch (direction)
            {
                case Enums.Direction.Up:
                    {
                        temp = flatland.grid[locationX][(locationY - 1 + flatland.n) % flatland.n];
                        break;
                    }
                case Enums.Direction.Right:
                    {
                        temp = flatland.grid[(locationX + 1) % flatland.n][locationY];
                        break;
                    }
                case Enums.Direction.Down:
                    {
                        temp = flatland.grid[locationX][(locationY + 1) % flatland.n];
                        break;
                    }
                case Enums.Direction.Left:
                    {
                        temp = flatland.grid[(locationX - 1 + flatland.n) % flatland.n][locationY];
                        break;
                    }
            }
            return temp;
        }

        public void move()
        {
            switch (direction)
            {
                case Enums.Direction.Up:
                    {
                        locationY = (locationY + gridSize - 1) % gridSize;
                        break;
                    }
                case Enums.Direction.Right:
                    {
                        locationX = (locationX + 1) % gridSize;
                        break;
                    }
                case Enums.Direction.Down:
                    {
                        locationY = (locationY + 1) % gridSize;
                        break;
                    }
                case Enums.Direction.Left:
                    {
                        locationX = (locationX + gridSize - 1) % gridSize;
                        break;
                    }
            }
        }

        public double calculateFitness()
        {
            if (Program.form.TrainOn5Scenarios && Program.form.currentFlatland == Program.form.flatlands.Length)
            {
                Program.form.currentFlatland = 0;
                return 0;
            }
            Flatland temp = Program.form.flatlands[Program.form.currentFlatland].makeACopy();
            restartPosition();
            int maxSteps = 60;
            double fitness = 0;
            for (int i = 0; i < maxSteps; i++)
            {
                setDirections(getLeftTile(), getForwardTile(), getRightTile());
                switch (getForwardTile())
                {
                    case Enums.TileContent.Food:
                        {
                            fitness += 5;
                            break;
                        }
                    case Enums.TileContent.Poison:
                        {
                            fitness -= 30;
                            break;
                        }
                }
                move();
                flatland.grid[locationX][locationY] = Enums.TileContent.Nothing;
            }
            flatland = temp;

            if (Program.form.TrainOn5Scenarios)
            {
                Program.form.currentFlatland++;
                return fitness + calculateFitness();
            }
            else
            {
                return fitness;
            }
        }

        public bool setDirections(Enums.TileContent left, Enums.TileContent forward, Enums.TileContent right)
        {
            //change direction ANN!
            double[] inputs = new double[6];
            double[] outputs = new double[3];
            if (forward == Enums.TileContent.Food)
                inputs[0] = 1;
            if (forward == Enums.TileContent.Poison)
                inputs[3] = 1;

            if (left == Enums.TileContent.Food)
                inputs[1] = 1;
            if (left == Enums.TileContent.Poison)
                inputs[4] = 1;

            if (right == Enums.TileContent.Food)
                inputs[2] = 1;
            if (right == Enums.TileContent.Poison)
                inputs[5] = 1;

            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 6; j++)
                {
                    outputs[i] = outputs[i] + inputs[j] * weights.arrayOfWeights[i * 6 + j];
                }
            }
            int max = 0;
            for (int i = 1; i < 3; i++)
            {
                if (outputs[i] > outputs[max])
                {
                    max = i;
                }
            }

            if (max == 1)
            {
                turnLeft();
            }
            if (max == 2)
            {
                turnRight();
            }
            return true;
        }
    }
}
