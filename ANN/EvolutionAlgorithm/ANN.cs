﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EvolutionAlgorithm.Phenotype;

namespace EvolutionAlgorithm
{
    public partial class ANN : Form
    {
        public Population population;
        public int numberOfWeights = 18;
        public Agent agent;
        public Flatland[] flatlands;
        public int currentFlatland;
        public bool drawing = false;
        public bool dinamicFlatland;
        public bool TrainOn5Scenarios;

        public string stringAverageFitness = "Average Fitness";
        public string stringdeviation = "Standard Deviation";
        public string stringMaxFitness = "Max Fitness";


        public ANN()
        {
            InitializeComponent();
        }

        private void Initialize()
        {
            //chart
            chart1.Series[stringAverageFitness].Points.Clear();
            chart1.Series[stringMaxFitness].Points.Clear();
            chart1.Series[stringdeviation].Points.Clear();
            chart1.ChartAreas[0].AxisX.MajorGrid.LineWidth = 0;
            chart1.ChartAreas[0].AxisY.MajorGrid.LineWidth = 0;
           
            int weightBitLenght;
            int populationSize;
            Program.elitism = true;
            double probabilityFood;
            double probabilityPoison;
            dinamicFlatland = chkDinamyc.Checked;
            TrainOn5Scenarios = chb5scenarios.Checked;

            currentFlatland = 0;

            try
            {
                weightBitLenght = int.Parse(txtWeightBitLength.Text);
                populationSize = int.Parse(txtPopulationSize.Text);
                Program.mutationRate = double.Parse(txtMutationRate.Text);
                Program.crossOverRate = double.Parse(txtCrossOverRate.Text);
                Program.tournamentSelectionRate = double.Parse(txtTournamentSelectionRate.Text);
                probabilityFood = double.Parse(txtFoodProbability.Text);
                probabilityPoison = double.Parse(txtPoisonProbability.Text);
            }
            catch (Exception)
            {
                weightBitLenght = 10;
                populationSize = 50;
                Program.mutationRate = 0.001;
                Program.crossOverRate = 0.8;
                Program.tournamentSelectionRate = 0.2;
                probabilityPoison = 0.33;
                probabilityFood = 0.33;
            }

            flatlands = new Flatland[TrainOn5Scenarios?5:1];
            cmbScenario.Items.Clear();
            for (int i = 0; i < flatlands.Length; i++)
            {
                flatlands[i] = new Flatland(probabilityFood, probabilityPoison);
                cmbScenario.Items.Add(i);
            }
            cmbScenario.Items.Add("Random");
            agent = new Agent();
            agent.flatland = flatlands[currentFlatland].makeACopy();

            population = new Population(numberOfWeights, weightBitLenght,
                (Enums.AdultSelection)cmbAdultSelection.SelectedIndex,
                (Enums.ParentSelection)cmbMateSelection.SelectedIndex, populationSize);
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void btnBegin_Click(object sender, EventArgs e)
        {
            Initialize();
            while (population.generation<100)
            {
                population.Reproduce();
                
                //chart
                chart1.Series[stringMaxFitness].Points.AddXY(population.generation,
                    population.maxFitness[population.generation - 1]);
                chart1.Series[stringdeviation].Points.AddXY(population.generation,
                    population.standardDeviation[population.generation - 1]);
                chart1.Series[stringAverageFitness].Points.AddXY(population.generation,
                    population.averageFitness[population.generation - 1]);
            }
            lblSteps.Text = "You can test the best one!";
            agent.restartPosition();
            this.Refresh();
        }

        private void ANN_Load(object sender, EventArgs e)
        {

        }

        protected override void OnPaint(PaintEventArgs e)
        {
            System.Drawing.Pen pen = new System.Drawing.Pen(Color.Black, 1);
            System.Drawing.SolidBrush brush = new System.Drawing.SolidBrush(System.Drawing.Color.Red);
            System.Drawing.Graphics formGraphics;
            formGraphics = this.CreateGraphics();

            int gridSize = 10;
            int stepX = gridPanel.Width / gridSize;
            int stepY = gridPanel.Height / gridSize;


            for (int i = 0; i < gridSize + 1; i++)
            {
                //vertikalna
                formGraphics.DrawLine(pen, gridPanel.Location.X + i * stepX, gridPanel.Location.Y,
                gridPanel.Location.X + i * stepX, gridPanel.Location.Y + gridPanel.Height / gridSize * gridSize);
                //horizontalna
                formGraphics.DrawLine(pen, gridPanel.Location.X, gridPanel.Location.Y + i * stepY,
                gridPanel.Location.X + gridPanel.Width / gridSize * gridSize, gridPanel.Location.Y + i * stepY);
            }
            if (!drawing)
            {
                return;
            }

            //food and poison
            int margine = 20;
            if (agent == null)
            {
                pen.Dispose();
                formGraphics.Dispose();
                return;
            }
            for (int i = 0; i < gridSize; i++)
            {
                for (int j = 0; j < gridSize; j++)
                {
                    switch (agent.flatland.grid[i][j])
                    {
                        case Enums.TileContent.Nothing:
                            {
                                break;
                            }
                        case Enums.TileContent.Food:
                            {
                                brush.Color = Color.Green;
                                formGraphics.FillEllipse(brush, new Rectangle(gridPanel.Location.X + i * stepX + margine / 2, gridPanel.Location.Y + j * stepY + margine / 2, stepX - margine, stepY - margine));
                                break;
                            }
                        case Enums.TileContent.Poison:
                            {
                                brush.Color = Color.Red;
                                formGraphics.FillEllipse(brush, new Rectangle(gridPanel.Location.X + i * stepX + margine / 2, gridPanel.Location.Y + j * stepY + margine / 2, stepX - margine, stepY - margine));
                                break;
                            }
                    }
                }
            }

            //agent
            pen.Width = 3;
            if (agent.direction != Enums.Direction.Down)
            {//up
                formGraphics.DrawLine(pen, gridPanel.Location.X + agent.locationX * stepX + stepX / 2, gridPanel.Location.Y + agent.locationY * stepY + stepY / 2,
                gridPanel.Location.X + agent.locationX * stepX + stepX / 2, gridPanel.Location.Y + agent.locationY * stepY);
            }

            if (agent.direction != Enums.Direction.Up)
            {//down
                formGraphics.DrawLine(pen, gridPanel.Location.X + agent.locationX * stepX + stepX / 2, gridPanel.Location.Y + agent.locationY * stepY + stepY / 2,
                gridPanel.Location.X + agent.locationX * stepX + stepX / 2, gridPanel.Location.Y + agent.locationY * stepY + stepY);
            }
            if (agent.direction != Enums.Direction.Left)
            {//right
                formGraphics.DrawLine(pen, gridPanel.Location.X + agent.locationX * stepX + stepX / 2, gridPanel.Location.Y + agent.locationY * stepY + stepY / 2,
                gridPanel.Location.X + agent.locationX * stepX + stepX, gridPanel.Location.Y + agent.locationY * stepY + stepY / 2);
            }
            if (agent.direction != Enums.Direction.Right)
            {//left
                formGraphics.DrawLine(pen, gridPanel.Location.X + agent.locationX * stepX + stepX / 2, gridPanel.Location.Y + agent.locationY * stepY + stepY / 2,
                gridPanel.Location.X + agent.locationX * stepX, gridPanel.Location.Y + agent.locationY * stepY + stepY / 2);
            }
            brush.Color = Color.Blue;
            formGraphics.FillEllipse(brush, new Rectangle(gridPanel.Location.X + agent.locationX * stepX + stepX / 4, gridPanel.Location.Y + agent.locationY * stepY + stepY / 4, stepX / 2, stepY / 2));

            pen.Dispose();
            formGraphics.Dispose();
        }

        private void btnTest_Click(object sender, EventArgs e)
        {
            drawing = true;

            agent.weights = (Ph_ANN)population.children[0];
            agent.flatland = flatlands[currentFlatland].makeACopy(); //ovde ne valja za random
            agent.restartPosition(); 
            Refresh();

            int food = 0, poison = 0;
            for (int i = 0; i < 60; i++)
            {
                lblSteps.Text = "Steps: " + i + ", Food: " + food + ", Poison: " + poison;
                agent.setDirections(agent.getLeftTile(), agent.getForwardTile(), agent.getRightTile());
                agent.move();
                if (agent.flatland.grid[agent.locationX][agent.locationY] == Enums.TileContent.Food)
                {
                    food++;
                }
                else if (agent.flatland.grid[agent.locationX][agent.locationY] == Enums.TileContent.Poison)
                {
                    poison++;
                }
                agent.flatland.grid[agent.locationX][agent.locationY] = Enums.TileContent.Nothing;
                this.Refresh();
                System.Threading.Thread.Sleep(500);
            }
            lblSteps.Text = "Done!" + " Food: " + food + ", Poison: " + poison;
        }

        private void cmbMateSelection_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbMateSelection.SelectedIndex == 2)
            {
                txtTournamentSelectionRate.Visible = true;
                lblTournamentSelRate.Visible = true;
            }
            else
            {
                txtTournamentSelectionRate.Visible = false;
                lblTournamentSelRate.Visible = false;
            }
        }

        private void cmbScenario_SelectedIndexChanged(object sender, EventArgs e)
        {
            drawing = true;
            if (agent == null)
            {
                return;
            }
            if (cmbScenario.SelectedIndex+1>flatlands.Length)
            {
                flatlands[currentFlatland] = new Flatland(flatlands[0].probabilityFood, flatlands[0].probabilityPoison, flatlands[0].n, true);
            }
            else
            {
                agent.flatland= flatlands[cmbScenario.SelectedIndex].makeACopy();
            }
            agent.restartPosition();
            Refresh();
        }
    }
}

