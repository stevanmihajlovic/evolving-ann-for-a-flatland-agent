﻿namespace EvolutionAlgorithm
{
    partial class ANN
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblCrossOverRate = new System.Windows.Forms.Label();
            this.lblMutationRate = new System.Windows.Forms.Label();
            this.txtCrossOverRate = new System.Windows.Forms.TextBox();
            this.txtMutationRate = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtWeightBitLength = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtPopulationSize = new System.Windows.Forms.TextBox();
            this.cmbMateSelection = new System.Windows.Forms.ComboBox();
            this.cmbAdultSelection = new System.Windows.Forms.ComboBox();
            this.lblTournamentSelRate = new System.Windows.Forms.Label();
            this.txtTournamentSelectionRate = new System.Windows.Forms.TextBox();
            this.btnBegin = new System.Windows.Forms.Button();
            this.txtFoodProbability = new System.Windows.Forms.TextBox();
            this.txtPoisonProbability = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.gridPanel = new System.Windows.Forms.Panel();
            this.btnTest = new System.Windows.Forms.Button();
            this.lblSteps = new System.Windows.Forms.Label();
            this.cmbScenario = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.chkDinamyc = new System.Windows.Forms.CheckBox();
            this.chb5scenarios = new System.Windows.Forms.CheckBox();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 156);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(114, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Parent/mate selection:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 119);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Adult selection:";
            // 
            // lblCrossOverRate
            // 
            this.lblCrossOverRate.AutoSize = true;
            this.lblCrossOverRate.Location = new System.Drawing.Point(118, 81);
            this.lblCrossOverRate.Name = "lblCrossOverRate";
            this.lblCrossOverRate.Size = new System.Drawing.Size(78, 13);
            this.lblCrossOverRate.TabIndex = 11;
            this.lblCrossOverRate.Text = "Cross-over rate";
            // 
            // lblMutationRate
            // 
            this.lblMutationRate.AutoSize = true;
            this.lblMutationRate.Location = new System.Drawing.Point(13, 81);
            this.lblMutationRate.Name = "lblMutationRate";
            this.lblMutationRate.Size = new System.Drawing.Size(72, 13);
            this.lblMutationRate.TabIndex = 12;
            this.lblMutationRate.Text = "Mutation rate:";
            // 
            // txtCrossOverRate
            // 
            this.txtCrossOverRate.Location = new System.Drawing.Point(118, 96);
            this.txtCrossOverRate.Name = "txtCrossOverRate";
            this.txtCrossOverRate.Size = new System.Drawing.Size(100, 20);
            this.txtCrossOverRate.TabIndex = 5;
            this.txtCrossOverRate.Text = "0,9";
            // 
            // txtMutationRate
            // 
            this.txtMutationRate.Location = new System.Drawing.Point(13, 96);
            this.txtMutationRate.Name = "txtMutationRate";
            this.txtMutationRate.Size = new System.Drawing.Size(100, 20);
            this.txtMutationRate.TabIndex = 6;
            this.txtMutationRate.Text = "0,01";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(118, 7);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(87, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "Weight bit length";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // txtWeightBitLength
            // 
            this.txtWeightBitLength.Location = new System.Drawing.Point(118, 22);
            this.txtWeightBitLength.Name = "txtWeightBitLength";
            this.txtWeightBitLength.Size = new System.Drawing.Size(100, 20);
            this.txtWeightBitLength.TabIndex = 7;
            this.txtWeightBitLength.Text = "10";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 13);
            this.label1.TabIndex = 14;
            this.label1.Text = "Population size:";
            // 
            // txtPopulationSize
            // 
            this.txtPopulationSize.Location = new System.Drawing.Point(13, 22);
            this.txtPopulationSize.Name = "txtPopulationSize";
            this.txtPopulationSize.Size = new System.Drawing.Size(100, 20);
            this.txtPopulationSize.TabIndex = 8;
            this.txtPopulationSize.Text = "10";
            // 
            // cmbMateSelection
            // 
            this.cmbMateSelection.FormattingEnabled = true;
            this.cmbMateSelection.Items.AddRange(new object[] {
            "Fitness-proportionate",
            "Sigma-scaling",
            "Tournament selection",
            "Rank Selection"});
            this.cmbMateSelection.Location = new System.Drawing.Point(13, 170);
            this.cmbMateSelection.Name = "cmbMateSelection";
            this.cmbMateSelection.Size = new System.Drawing.Size(183, 21);
            this.cmbMateSelection.TabIndex = 3;
            this.cmbMateSelection.SelectedIndexChanged += new System.EventHandler(this.cmbMateSelection_SelectedIndexChanged);
            // 
            // cmbAdultSelection
            // 
            this.cmbAdultSelection.FormattingEnabled = true;
            this.cmbAdultSelection.Items.AddRange(new object[] {
            "Full Generational Replacement",
            "Over-production",
            "Generational Mixing"});
            this.cmbAdultSelection.Location = new System.Drawing.Point(13, 133);
            this.cmbAdultSelection.Name = "cmbAdultSelection";
            this.cmbAdultSelection.Size = new System.Drawing.Size(183, 21);
            this.cmbAdultSelection.TabIndex = 4;
            // 
            // lblTournamentSelRate
            // 
            this.lblTournamentSelRate.AutoSize = true;
            this.lblTournamentSelRate.Location = new System.Drawing.Point(13, 193);
            this.lblTournamentSelRate.Name = "lblTournamentSelRate";
            this.lblTournamentSelRate.Size = new System.Drawing.Size(107, 13);
            this.lblTournamentSelRate.TabIndex = 16;
            this.lblTournamentSelRate.Text = "Tournament sel. rate:";
            this.lblTournamentSelRate.Visible = false;
            // 
            // txtTournamentSelectionRate
            // 
            this.txtTournamentSelectionRate.Location = new System.Drawing.Point(13, 207);
            this.txtTournamentSelectionRate.Name = "txtTournamentSelectionRate";
            this.txtTournamentSelectionRate.Size = new System.Drawing.Size(100, 20);
            this.txtTournamentSelectionRate.TabIndex = 15;
            this.txtTournamentSelectionRate.Text = "0,2";
            this.txtTournamentSelectionRate.Visible = false;
            // 
            // btnBegin
            // 
            this.btnBegin.Location = new System.Drawing.Point(132, 198);
            this.btnBegin.Name = "btnBegin";
            this.btnBegin.Size = new System.Drawing.Size(75, 23);
            this.btnBegin.TabIndex = 17;
            this.btnBegin.Text = "Begin";
            this.btnBegin.UseVisualStyleBackColor = true;
            this.btnBegin.Click += new System.EventHandler(this.btnBegin_Click);
            // 
            // txtFoodProbability
            // 
            this.txtFoodProbability.Location = new System.Drawing.Point(13, 59);
            this.txtFoodProbability.Name = "txtFoodProbability";
            this.txtFoodProbability.Size = new System.Drawing.Size(100, 20);
            this.txtFoodProbability.TabIndex = 6;
            this.txtFoodProbability.Text = "0,33";
            // 
            // txtPoisonProbability
            // 
            this.txtPoisonProbability.Location = new System.Drawing.Point(119, 59);
            this.txtPoisonProbability.Name = "txtPoisonProbability";
            this.txtPoisonProbability.Size = new System.Drawing.Size(100, 20);
            this.txtPoisonProbability.TabIndex = 5;
            this.txtPoisonProbability.Text = "0,33";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "Food probability:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(119, 44);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(89, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Poison probability";
            // 
            // gridPanel
            // 
            this.gridPanel.Location = new System.Drawing.Point(251, 12);
            this.gridPanel.Name = "gridPanel";
            this.gridPanel.Size = new System.Drawing.Size(300, 300);
            this.gridPanel.TabIndex = 18;
            this.gridPanel.Visible = false;
            // 
            // btnTest
            // 
            this.btnTest.Location = new System.Drawing.Point(590, 44);
            this.btnTest.Name = "btnTest";
            this.btnTest.Size = new System.Drawing.Size(89, 23);
            this.btnTest.TabIndex = 19;
            this.btnTest.Text = "Test the best!";
            this.btnTest.UseVisualStyleBackColor = true;
            this.btnTest.Click += new System.EventHandler(this.btnTest_Click);
            // 
            // lblSteps
            // 
            this.lblSteps.AutoSize = true;
            this.lblSteps.Location = new System.Drawing.Point(587, 28);
            this.lblSteps.Name = "lblSteps";
            this.lblSteps.Size = new System.Drawing.Size(32, 13);
            this.lblSteps.TabIndex = 20;
            this.lblSteps.Text = "Step:";
            // 
            // cmbScenario
            // 
            this.cmbScenario.FormattingEnabled = true;
            this.cmbScenario.Location = new System.Drawing.Point(571, 95);
            this.cmbScenario.Name = "cmbScenario";
            this.cmbScenario.Size = new System.Drawing.Size(183, 21);
            this.cmbScenario.TabIndex = 4;
            this.cmbScenario.SelectedIndexChanged += new System.EventHandler(this.cmbScenario_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(571, 81);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 13);
            this.label7.TabIndex = 10;
            this.label7.Text = "Scenario:";
            // 
            // chkDinamyc
            // 
            this.chkDinamyc.AutoSize = true;
            this.chkDinamyc.Location = new System.Drawing.Point(571, 122);
            this.chkDinamyc.Name = "chkDinamyc";
            this.chkDinamyc.Size = new System.Drawing.Size(101, 17);
            this.chkDinamyc.TabIndex = 21;
            this.chkDinamyc.Text = "Dinamic flatland";
            this.chkDinamyc.UseVisualStyleBackColor = true;
            // 
            // chb5scenarios
            // 
            this.chb5scenarios.AutoSize = true;
            this.chb5scenarios.Location = new System.Drawing.Point(571, 145);
            this.chb5scenarios.Name = "chb5scenarios";
            this.chb5scenarios.Size = new System.Drawing.Size(122, 17);
            this.chb5scenarios.TabIndex = 21;
            this.chb5scenarios.Text = "Train on 5 scenarios";
            this.chb5scenarios.UseVisualStyleBackColor = true;
            // 
            // chart1
            // 
            chartArea1.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea1);
            this.chart1.DataSource = this.chart1.Legends;
            legend1.Name = "Legend1";
            this.chart1.Legends.Add(legend1);
            this.chart1.Location = new System.Drawing.Point(16, 318);
            this.chart1.Name = "chart1";
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series1.Color = System.Drawing.Color.Red;
            series1.Legend = "Legend1";
            series1.Name = "Max Fitness";
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series2.Color = System.Drawing.Color.Blue;
            series2.Legend = "Legend1";
            series2.Name = "Standard Deviation";
            series3.ChartArea = "ChartArea1";
            series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series3.Color = System.Drawing.Color.Lime;
            series3.Legend = "Legend1";
            series3.Name = "Average Fitness";
            this.chart1.Series.Add(series1);
            this.chart1.Series.Add(series2);
            this.chart1.Series.Add(series3);
            this.chart1.Size = new System.Drawing.Size(892, 279);
            this.chart1.TabIndex = 22;
            this.chart1.Text = "chart1";
            // 
            // ANN
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(925, 609);
            this.Controls.Add(this.chart1);
            this.Controls.Add(this.chb5scenarios);
            this.Controls.Add(this.chkDinamyc);
            this.Controls.Add(this.lblSteps);
            this.Controls.Add(this.btnTest);
            this.Controls.Add(this.gridPanel);
            this.Controls.Add(this.btnBegin);
            this.Controls.Add(this.lblTournamentSelRate);
            this.Controls.Add(this.txtTournamentSelectionRate);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lblCrossOverRate);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtPoisonProbability);
            this.Controls.Add(this.lblMutationRate);
            this.Controls.Add(this.txtFoodProbability);
            this.Controls.Add(this.txtCrossOverRate);
            this.Controls.Add(this.txtMutationRate);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtWeightBitLength);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtPopulationSize);
            this.Controls.Add(this.cmbScenario);
            this.Controls.Add(this.cmbMateSelection);
            this.Controls.Add(this.cmbAdultSelection);
            this.Name = "ANN";
            this.Text = "ANN";
            this.Load += new System.EventHandler(this.ANN_Load);
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblCrossOverRate;
        private System.Windows.Forms.Label lblMutationRate;
        private System.Windows.Forms.TextBox txtCrossOverRate;
        private System.Windows.Forms.TextBox txtMutationRate;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtWeightBitLength;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtPopulationSize;
        private System.Windows.Forms.ComboBox cmbMateSelection;
        private System.Windows.Forms.ComboBox cmbAdultSelection;
        private System.Windows.Forms.Label lblTournamentSelRate;
        private System.Windows.Forms.TextBox txtTournamentSelectionRate;
        private System.Windows.Forms.Button btnBegin;
        private System.Windows.Forms.TextBox txtFoodProbability;
        private System.Windows.Forms.TextBox txtPoisonProbability;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel gridPanel;
        private System.Windows.Forms.Button btnTest;
        private System.Windows.Forms.Label lblSteps;
        private System.Windows.Forms.ComboBox cmbScenario;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox chkDinamyc;
        private System.Windows.Forms.CheckBox chb5scenarios;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
    }
}