﻿namespace EvolutionAlgorithm
{
    public class Enums
    {
        public enum AdultSelection
        {
            Full,
            OverProduction,
            Mixing
        }

        public enum ParentSelection
        {
            FitnessProportionate,
            SigmaScaling,
            TournamentSelection,
            RankSelection
        }

        public enum Phenotype
        {
            OneMaxProblem,
            LolzProblem,
            SurpisingSequenceLocally,
            SurpisingSequenceGlobally,
            ANN
        }

        public enum TileContent
        {
            Nothing,
            Food,
            Poison
        }

        public enum Direction
        {
            Up,
            Right,
            Down,
            Left
        }
    }
}