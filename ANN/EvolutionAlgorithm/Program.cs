﻿using System;
using System.Windows.Forms;

namespace EvolutionAlgorithm
{
    internal static class Program
    {
        /// <summary>
        ///     The main entry point for the application.
        /// </summary>
        public static ANN form;

        public static Random rnd;
        public static double mutationRate = 0.01;
        public static double crossOverRate = 0.9;
        public static bool elitism;
        public static double tournamentSelectionRate;
        public static double z = 4;
        public static int rangeForSurprising;

        [STAThread]
        private static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            rnd = new Random();
            Application.Run(form = new ANN());
        }
    }
}