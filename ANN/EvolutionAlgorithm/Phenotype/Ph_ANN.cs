﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvolutionAlgorithm.Phenotype
{
    public class Ph_ANN : GenotypeBitArray
    {
        public double fitness;
        public int doubleLength;
        public double[] arrayOfWeights;

        public Ph_ANN(int numberOfWeights, bool randomize = true, int DoubleLength = 10) : base(numberOfWeights * DoubleLength, randomize)
        {
            arrayOfWeights = new double[numberOfWeights];
            doubleLength = DoubleLength;

            if (randomize)
                updateWeights();
        }

        public void updateWeights()
        {
            int k;
            for (int i = 0; i < arrayOfWeights.Length; i++)
            {
                double value = 0;
                k = 1;
                for (int j = 0; j < doubleLength; j++)
                {
                    if (genes.Get(i * doubleLength + j))
                    {
                        value += k;
                    }
                    k = k << 1;
                }
                arrayOfWeights[i] = value / ((double)k - 1);
            }
        }
        public override void CalculateFitness()
        {
            if (arrayOfWeights == null)
            {
                return;
            }
            Program.form.agent.weights = this;
            this.updateWeights();
            fitness = Program.form.agent.calculateFitness();
        }

        public override double GetFitness()
        {
            return fitness;
        }

        public override GenotypeInterface New()
        {
            GenotypeInterface returnGenotypeInterface = new Ph_ANN(arrayOfWeights.Length, false,this.doubleLength);
            return returnGenotypeInterface;
        }

        public override void SetFitness(double newFitness)
        {
            fitness = newFitness;
        }
    }
}
