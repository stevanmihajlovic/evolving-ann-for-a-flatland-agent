﻿namespace EvolutionAlgorithm.Phenotype
{
    public abstract class GenotypeIntArray : GenotypeInterface
    {
        public int[] genes;

        public GenotypeIntArray()
        {
        }

        public GenotypeIntArray(int size, bool randomize)
        {
            var rnd = Program.rnd;
            genes = new int[size];
            if (!randomize) return;
            for (var i = 0; i < size; i++)
            {
                genes[i] = (int) (rnd.NextDouble()*Program.rangeForSurprising);
            }
            CalculateFitness();
        }

        public abstract void CalculateFitness();

        public GenotypeInterface[] CrossOver(GenotypeInterface second)
        {
            GenotypeInterface first = this;
            var returnGenotypeIntArray = new GenotypeInterface[2];
            returnGenotypeIntArray[0] = first.New();
            returnGenotypeIntArray[1] = first.New();

            if (Program.rnd.NextDouble() > Program.crossOverRate)
            {
                for (var i = 0; i < ((GenotypeIntArray) first).genes.Length; i++)
                {
                    ((GenotypeIntArray) returnGenotypeIntArray[0]).genes[i] = ((GenotypeIntArray) first).genes[i];
                    ((GenotypeIntArray) returnGenotypeIntArray[1]).genes[i] = ((GenotypeIntArray) second).genes[i];
                }
                returnGenotypeIntArray[0].CalculateFitness();
                returnGenotypeIntArray[1].CalculateFitness();
                return returnGenotypeIntArray;
            }
            var change = Program.rnd.Next(((GenotypeIntArray) first).genes.Length);

            for (var i = 0; i < ((GenotypeIntArray) first).genes.Length; i++)
            {
                if (i < change)
                {
                    ((GenotypeIntArray) returnGenotypeIntArray[0]).genes[i] = ((GenotypeIntArray) first).genes[i];
                    ((GenotypeIntArray) returnGenotypeIntArray[1]).genes[i] = ((GenotypeIntArray) second).genes[i];
                }
                else
                {
                    ((GenotypeIntArray) returnGenotypeIntArray[0]).genes[i] = ((GenotypeIntArray) second).genes[i];
                    ((GenotypeIntArray) returnGenotypeIntArray[1]).genes[i] = ((GenotypeIntArray) first).genes[i];
                }
            }
            returnGenotypeIntArray[0].Mutation();
            returnGenotypeIntArray[1].Mutation();
            returnGenotypeIntArray[0].CalculateFitness();
            returnGenotypeIntArray[1].CalculateFitness();
            return returnGenotypeIntArray;
        }

        public abstract double GetFitness();

        public void Mutation()
        {
            var rnd = Program.rnd;
            for (var i = 0; i < genes.Length; i++)
            {
                if (rnd.NextDouble() < Program.mutationRate)
                {
                    genes[i] = (int) (rnd.NextDouble()*Program.rangeForSurprising);
                }
            }
        }

        public abstract GenotypeInterface New();

        public string Print()
        {
            var returnString = "";
            for (var i = 0; i < genes.Length; i++)
            {
                returnString += genes[i] + ", ";
            }
            return returnString;
        }

        public abstract void SetFitness(double newFitness);
    }
}