﻿namespace EvolutionAlgorithm.Adult_Selection
{
    public interface Adult
    {
        void Selection(Population population);
        void MakeChildren(Population population);
    }
}