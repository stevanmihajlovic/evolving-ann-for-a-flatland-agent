﻿using System.Linq;

namespace EvolutionAlgorithm.Adult_Selection
{
    public class FullGenerationalReplacement : Adult
    {
        public void Selection(Population population)
        {
            population.adults.Clear();
            foreach (var item in population.children)
            {
                population.adults.Add(item);
            }
        }

        public void MakeChildren(Population population)
        {
            population.children.Clear();
            population.adults = population.adults.OrderByDescending(item => item.GetFitness()).ToList();
            if (Program.elitism)
            {
                population.children.Add(population.adults[0]);
            }
            for (var i = 0; i < population.populationSize - (Program.elitism ? 1 : 0); i++)
            {
                var first = population.Parent.Selection(population);
                var second = population.Parent.Selection(population);
                var kids = first.CrossOver(second);
                kids[0].CalculateFitness();
                kids[1].CalculateFitness();
                population.children.Add(kids[kids[0].GetFitness() > kids[1].GetFitness() ? 0 : 1]);
            }
            population.children = population.children.OrderByDescending(item => item.GetFitness()).ToList();
        }
    }
}