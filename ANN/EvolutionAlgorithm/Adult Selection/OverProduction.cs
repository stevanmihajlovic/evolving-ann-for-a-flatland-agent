﻿using System.Linq;

namespace EvolutionAlgorithm.Adult_Selection
{
    internal class OverProduction : Adult
    {
        public void Selection(Population population)
        {
            population.children = population.children.OrderByDescending(item => item.GetFitness()).ToList();
            population.children.RemoveRange(population.populationSize,
                population.children.Count - population.populationSize);
            population.adults.Clear();
            foreach (var item in population.children)
            {
                population.adults.Add(item);
            }
        }

        public void MakeChildren(Population population)
        {
            population.children.Clear();
            population.adults = population.adults.OrderByDescending(item => item.GetFitness()).ToList();
            if (Program.elitism && population.adults.Count > 2)
            {
                population.children.Add(population.adults[0]);
                population.children.Add(population.adults[1]);
            }

            for (var i = 0; i < population.populationSize*2 - (Program.elitism ? 2 : 0); i++)
            {
                var first = population.Parent.Selection(population);
                var second = population.Parent.Selection(population);
                var kids = first.CrossOver(second);
                kids[0].CalculateFitness();
                kids[1].CalculateFitness();
                population.children.Add(kids[kids[0].GetFitness() > kids[1].GetFitness() ? 0 : 1]);
            }
            population.children = population.children.OrderByDescending(item => item.GetFitness()).ToList();
        }
    }
}