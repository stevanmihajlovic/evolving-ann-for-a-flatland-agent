﻿using System.Collections.Generic;
using System.Linq;
using EvolutionAlgorithm.Phenotype;

namespace EvolutionAlgorithm.Parent_Selection
{
    internal class TournamentSelection : Parent
    {
        public GenotypeInterface Selection(Population population)
        {
            var randomAdults = new List<GenotypeInterface>();
            for (var i = 0; i < Program.tournamentSelectionRate*population.adults.Count; i++)
            {
                randomAdults.Add(population.adults[Program.rnd.Next(population.populationSize)]);
            }
            randomAdults = randomAdults.OrderByDescending(item => item.GetFitness()).ToList();
            return randomAdults[0];
        }
    }
}