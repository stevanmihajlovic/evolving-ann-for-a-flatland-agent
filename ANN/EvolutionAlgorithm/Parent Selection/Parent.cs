﻿using EvolutionAlgorithm.Phenotype;

namespace EvolutionAlgorithm.Parent_Selection
{
    public interface Parent
    {
        GenotypeInterface Selection(Population population);
    }
}