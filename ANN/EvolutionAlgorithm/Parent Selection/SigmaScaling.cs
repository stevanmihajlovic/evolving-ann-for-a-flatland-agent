﻿using System;
using EvolutionAlgorithm.Phenotype;

namespace EvolutionAlgorithm.Parent_Selection
{
    internal class SigmaScaling : Parent
    {
        public GenotypeInterface Selection(Population population)
        {
            double average = 0;
            double sum = 0;
            double standardDeviation = 0;
            foreach (var item in population.adults)
            {
                sum += item.GetFitness();
            }
            average = sum/population.adults.Count;

            foreach (var item in population.adults)
            {
                standardDeviation += Math.Pow(item.GetFitness() - average, 2);
                standardDeviation = Math.Sqrt(standardDeviation/population.adults.Count);
            }
            sum = 0;
            foreach (var item in population.adults)
            {
                if (standardDeviation == 0.0)
                {
                    item.SetFitness(1 +
                                    (item.GetFitness() - average)/
                                    (2*population.standardDeviation[population.generation - 1]));
                    sum += item.GetFitness();
                }
                else
                {
                    item.SetFitness(1 + (item.GetFitness() - average)/(2*standardDeviation));
                    sum += item.GetFitness();
                }
            }

            var randomNumber = Program.rnd.NextDouble()*sum;

            var i = 0;
            while (randomNumber > 0)
            {
                randomNumber -= population.adults[i].GetFitness();
                i++;
            }
            return population.adults[i < 1 ? 0 : i - 1];
        }
    }
}