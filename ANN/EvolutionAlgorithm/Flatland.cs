﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvolutionAlgorithm
{
    public class Flatland
    {
        public double probabilityFood;
        public double probabilityPoison;

        public int n;
        public Enums.TileContent[][] grid;

        public Flatland()
        {
            probabilityFood = 0.33;
            probabilityPoison = 0.33;
            n = 10;
            grid = new Enums.TileContent[n][];
            for (int i = 0; i < n; i++)
            {
                grid[i] = new Enums.TileContent[n];
            }
            randomizeGrid();

        }

        public Flatland(double probF = 0.33, double probP = 0.33, int N = 10, bool randomize=true)
        {
            probabilityFood = probF;
            probabilityPoison = probP;
            n = N;
            grid = new Enums.TileContent[n][];
            for (int i = 0; i < n; i++)
            {
                grid[i] = new Enums.TileContent[n];
            }
            if (randomize)
            {
                randomizeGrid();
            }
        }

        public void randomizeGrid()
        {
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    double temp = Program.rnd.NextDouble();
                    temp = temp - probabilityFood;
                    if (temp < 0)
                    {
                        grid[i][j] = Enums.TileContent.Food;
                        continue;
                    }
                    temp = temp - probabilityPoison;
                    if (temp < 0)
                    {
                        grid[i][j] = Enums.TileContent.Poison;
                    }
                    else
                    {
                        grid[i][j] = Enums.TileContent.Nothing;
                    }
                }
            }
        }

        public Flatland makeACopy()
        {
            Flatland temp=new Flatland(probabilityFood, probabilityPoison, n,false);
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    temp.grid[i][j] = this.grid[i][j];
                }
            }
            return temp;
        }
        
    }
}

